
const isNull = async (value) => {
    if(value == null){
        throw new Error("El valor no puede ser null -> " + value);
    }
}

const isEmpty = async (value) => {
    if(value.trim() == ''){
        throw new Error("El valor no puede ser vacío -> " + value);
    }
}

exports.isNotNumber = async (value) => {
    if (isNaN(value)){
        throw new Error("El valor tiene que ser número -> " + value);
    }
}

exports.higherThanZero = async (value) => {
    if (isNaN(value) || value <= 0){
        throw new Error("El valor tiene que ser número y mayor que cero -> " + value);
    }
}

exports.validatorBucle = async (object) => {
    for (const key in object) {
        if(key != 'uid' && key != 'kmrecorridos'){
            if (Object.hasOwnProperty.call(object, key)) { 
                await isNull(object[key]);
                await isEmpty(object[key]);
            }
        }
    }
}
