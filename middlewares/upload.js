const s3 = require('../config/bucket');
const multer = require('multer');
const multerS3 = require('multer-s3');

const storage = multerS3({ // notice you are calling the multer.diskStorage() method here, not multer()
    s3:s3,
    bucket:'jhkmultiserviciosfilesbucket1',
    key: function (req, file, cb){
        cb(null, file.originalname);
    }
});

const upload = multer({storage});

module.exports = {
    upload
};