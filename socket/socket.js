const {io} = require('../index'); 

const { comprobarJWT } = require('../helpers/jwt');
const { userConnection, userDisconnect } = require('../controllers/socket');
const { 
    vehiculoAdd, 
    exterioresAdd, 
    interioresAdd, 
    accesoriosAdd, 
    componentesAdd,
    ordenServicio,
    getVehiculoEtapas,
    getVehiculos
} = require('../controllers/socketVehiculo');

// Mensajes de sockets
io.on('connection', client => {

    console.log("cliente conectado") 
    /*
    const [valido, uid] = comprobarJWT(client.handshake.headers['x-token']);
    
    if( !valido ) {
        return client.disconnect();
    }
    //Cliente logueado
    userConnection(uid);
    
    //Ingresar al cliente a una sala
    client.join( uid );
    
    //Mensajes personales
    client.on('private-message', (payload) => {
        console.log(payload);
        io.to( payload.para ).emit('private-message', payload);
    });
    */

    // Crear vehiculo inicial orden de entradas al taller
    /* codigo front emit para crear
    socket.emit('orden-entrada',{uid:'', nombre:'JOAQUIN',telefono:'980', rfc:'RFC', modelo:'modelo', tipo:'TIPO',usuario:'608b1d786e8f6b38f0c13d16' });
    *EDITAR* -> SE AGREGA EL UID DEL VEHICULO
    socket.emit('orden-entrada',{uid:'60b83bfa48d25816dc02705d', marca:'Marca',color:'Azul', placa:'B11518U', nseries:'890TYE56', nmotor:'MR8912', kmrecorridos:125.5 });
    */
    client.on('orden-entrada', async(payload) =>{
        console.log(payload);
        const vehiculo = await vehiculoAdd(payload);
        client.emit('orden-entrada', vehiculo);
    });

    // AGREGAR TRUE O FALSE EXTERIORES
    /**
     * socket.emit('exteriores-entrada',{uid:'',uidVehiculo:'60b83bfa48d25816dc02705d', uluces:true, cuartosluces:true, antena:true, espejoLateral:true, cristales:true, emblea:true, llantas:true, taponRuedas:true, molduras:true, taponGasolina:true, carroceriaGolpes:true, bocinasClaxon:true, limpiadores:true});
     * 
     */
    client.on('exteriores-entrada', async(payload) =>{
        const exteriores = await exterioresAdd(payload);
        client.emit('exteriores-entrada', exteriores);
    });

    //Agregar true false INTERIORES
    /**
     * socket.emit('interiores-entrada',{uid:'',uidVehiculo:'60b83bfa48d25816dc02705d', itablero:true, calefaccion:true, radio:true, bocinas:true, encendedor:true, espejoRetrovisor:true, ceniceros:true, cinturones:true, botonesInteriores:true, manijasInteriores:true, tapetes:true, vestiduras:true});
     */
    client.on('interiores-entrada', async(payload)=> {
        const interiores = await interioresAdd(payload);
        client.emit('interiores-entrada', interiores);
    });

    /**
     * socket.emit('accesorios-entrada',{uid:'',uidVehiculo:'60b83bfa48d25816dc02705d', gato:true, maneralGato:true, llaveRuedas:true, estucheHerramientas:true, trianguloSeguridad:true, llantaRefaccion:true, extinguidor:true});
     */
    client.on('accesorios-entrada', async(payload)=> {
        const accesorios = await accesoriosAdd(payload);
        client.emit('accesorios-entrada', accesorios);
    });

    /**
     * socket.emit('componentes-entrada',{uid:'',uidVehiculo:'60b83bfa48d25816dc02705d', claxon:true, taponAceite:true, taponRadiador:true, varillaAceite:true, catalizador:true, bateria:true});
     */
    client.on('componentes-entrada', async(payload) => {
        const componentes = await componentesAdd(payload);
        client.emit('componentes-entrada', componentes);
    });

    /**
     * socket.emit('orden-servicio',{uid:'',uidVehiculo:'60b83bfa48d25816dc02705d', trabajoRealizar:'Se limpiara el motor le callo aceite', diagnostico:'El aceite callo de arriba hacia abajo', riesgos:'mevoy a aceitar la mano '});
     */
    client.on('orden-servicio', async(payload) => {
        const orden = await ordenServicio(payload);
        client.emit('order-servicio', orden);
    });
    
    /**
     * socket.emit('get-vehiculo',{uid:'60b83bfa48d25816dc02705d',etapa:5});
     */
    client.on('get-vehiculo', async(payload) => {
        const vehiculo = await getVehiculoEtapas(payload);
        console.log(vehiculo);
        client.emit('get-vehiculo', vehiculo);
    });

    /**
     * Traer todas las ordenes de entrada de vehiculos
     * 
     */
    client.on('get-vehiculos', async ()=>{
        const vehiculos = await getVehiculos();
        console.log(vehiculos);
        client.emit('get-vehiculos', vehiculos);
    });

    client.on('disconnect', () => { 
        console.log("cliente desconectado") 
        //userDisconnect(uid);
    });

});