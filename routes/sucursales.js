const { Router } = require('express');
const { check } = require('express-validator');

const { create, getSucursales } = require('../controllers/sucursales');

const { validarCampos } = require('../middlewares/validar-campos');
 const { validarJWT } = require('../middlewares/validar-jwt');
 
 const router = Router();

 router.post('/create',[
     check('nombre','Error el nombre es obligatorio').trim().not().isEmpty(),
     validarCampos,
     validarJWT
 ],create);

 router.get('/',validarJWT,getSucursales);

 module.exports = router;