/**
 * path : api/login
 */

const { Router } = require('express');
const { check } = require('express-validator');
const { crearUsuario, update, loginUsuario, renewToken } = require('../controllers/auth');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');

const router = Router();

router.post('/create', [
    check('name','Nombre es obligatorio').not().isEmpty(),
    check('email','Email es obligatorio').not().isEmpty(),
    check('email','Email debe tener formato correcto').isEmail().normalizeEmail(),
    check('password','Password es obligatorio').not().isEmpty(),
    check('sucursal','Sucursal uid es obligatorio').not().isEmpty(),
    validarCampos
],crearUsuario);
 
router.post('/',[
    check('email','Email es obligatorio').not().isEmpty(),
    check('email','Email debe tener formato correcto').isEmail().normalizeEmail(),
    check('password','Password es obligatorio').not().isEmpty(),
    validarCampos
], loginUsuario);


router.put('/update/:uid',[
    check('name','Nombre es obligatorio').not().isEmpty(),
    check('email','Email es obligatorio').not().isEmpty(),
    check('email','Email debe tener formato correcto').isEmail().normalizeEmail(),
    validarCampos,
    validarJWT
],update);

router.get('/renew', validarJWT, renewToken);

module.exports = router;