const { Router } = require('express');
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');
const { upload } = require('../middlewares/upload');
const {
    getVehiculos,
    getVehiculosHistorial,
    saveFileOrdenServicio,
    getPdfView,
    updatePdf,
    makeDocumentosFiscalia,
    getFiscalia
} = require('../controllers/vehiculo');

const router = Router();

// Get All
router.get('/:uid', validarJWT,getVehiculos);

router.get('/',validarJWT,getVehiculosHistorial);

router.post('/upload',[
    validarJWT
],saveFileOrdenServicio);

router.post('/pdfupdate',[
    check('uid','El uid no puede ir vacio').not().isEmpty(),
    check('opc', 'Envie la opcion de busqueda').not().isEmpty(),
    validarJWT
], updatePdf);

router.post('/donwloadpdf',[
    check('uid', 'Envie el documento que quiere buscar').not().isEmpty(),
    check('opc', 'Envie la opcion de busqueda').not().isEmpty(),
    validarCampos,
    validarJWT
], getPdfView);

router.post('/fiscalia', validarJWT, makeDocumentosFiscalia);

router.post('/docfiscalia', validarJWT, getFiscalia);

module.exports = router;