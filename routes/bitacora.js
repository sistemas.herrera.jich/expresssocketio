const { Router } = require('express');
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');
const Bitacora = require('../controllers/bitacora');
const router = Router();

router.post('/', [
    check('page', 'El número de pagina es obligatorio').not().isEmpty(),
    validarCampos,
    validarJWT
], Bitacora.getVehiculosHistorialPaginator);



module.exports = router;