const express = require('express');
const path = require('path');
const cors = require('cors');

require('dotenv').config();
//DB Config
require('./database/config').dbConnection();
// APP Express
const app = express();
//Lectura y parse del body 
app.use( express.json({limit: '50mb'}) );
app.use( express.urlencoded({ extended: true, limit: '50mb' }));

app.use(cors());
// Node: node socket io server
const server = require('http').createServer(app);
module.exports.io = require('socket.io')(server);
require('./socket/socket');

const port = process.env.PORT;
const publicPath = path.resolve( __dirname, 'public');
app.use( express.static(publicPath));

//Mis rutas
app.use('/api/login', require('./routes/auth'));
app.use('/api/usuarios', require('./routes/usuarios'));
app.use('/api/vehiculos', require('./routes/vehiculos'));
app.use('/api/sucursal', require('./routes/sucursales'));
app.use('/api/bitacora', require('./routes/bitacora'));

server.listen(port, (err)=>{
    if( err ) throw new Error(err);
})