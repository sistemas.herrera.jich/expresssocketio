const AWS = require('aws-sdk');

const s3 = new AWS.S3({
    accessKeyId:process.env.ID,
    secretAccessKey:process.env.SECRET,
    region:'us-west-1',
});

module.exports = s3;