const { Schema, model } = require('mongoose');

VehiculoExterioresSchema = Schema({
    uluces:{
        type:Boolean,
        default:false 
    },
    cuartosluces:{
        type:Boolean,
        default:false
    },
    antena:{
        type:Boolean,
        default:false
    },
    espejoLateral:{
        type:Boolean,
        default:false
    },
    cristales:{
        type:Boolean,
        default:false
    },
    emblema:{
        type:Boolean,
        default:false
    },
    llantas:{
        type:Boolean,
        default:false
    },
    taponRuedas:{
        type:Boolean,
        default:false
    },
    molduras:{
        type:Boolean,
        default:false
    },
    taponGasolina:{
        type:Boolean,
        default:false
    },
    carroceriaGolpes:{
        type:Boolean,
        default:false
    },
    bocinasClaxon:{
        type:Boolean,
        default:false
    },
    limpiadores:{
        type:Boolean,
        default:false
    }
});

VehiculoExterioresSchema.method('toJSON', function(){
    const { _id, __v, ...objects } = this.toObject();
    objects.uid = _id;
    return objects;
});

module.exports = model('Exteriores', VehiculoExterioresSchema, 'exteriores');