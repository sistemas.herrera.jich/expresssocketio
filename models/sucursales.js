const { Schema , model } = require('mongoose');

const SucursalSchema = Schema({
    nombre:{
        type:String,
        require:true,
        unique:true
    },
    descripcion:{
        type:String,
        default:'Suc. origin '
    },
    ciudad:{
        type:String,
        default:'Culiacán'
    },
    estado:{
        type:String,
        default:'Sinaloa'
    },
    status:{
        type:String,
        default:'ACTIVO' // 'ACTIVO' - 'INACTIVO'
    }
});

SucursalSchema.method('toJSON',function(){
    const { __v, _id, ...objects } = this.toObject();
    objects.uid = _id;
    return objects;
}); 

module.exports = model('Sucursal',SucursalSchema,'sucursal');