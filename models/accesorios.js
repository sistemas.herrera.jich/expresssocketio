const { Schema, model } = require('mongoose');

AccesoriosSchema = Schema({
    gato:{
        type:Boolean,
        default:false 
    },
    maneralGato:{
        type:Boolean,
        default:false
    },
    llaveRuedas:{
        type:Boolean,
        default:false
    },
    estucheHerramientas:{
        type:Boolean,
        default:false
    },
    trianguloSeguridad:{
        type:Boolean,
        default:false
    },
    llantaRefaccion:{
        type:Boolean,
        default:false
    },
    extinguidor:{
        type:Boolean,
        default:false
    }
});

AccesoriosSchema.method('toJSON', function(){
    const { _id, __v, ...objects } = this.toObject();
    objects.uid = _id;
    return objects;
});

module.exports = model('Accesorios', AccesoriosSchema, 'accesorios');