const { Schema, model } = require('mongoose');

VehiculoInterioresSchema = Schema({
    itablero:{
        type:Boolean,
        default:false 
    },
    calefaccion:{
        type:Boolean,
        default:false
    },
    radio:{
        type:Boolean,
        default:false
    },
    bocinas:{
        type:Boolean,
        default:false
    },
    encendedor:{
        type:Boolean,
        default:false
    },
    espejoRetrovisor:{
        type:Boolean,
        default:false
    },
    ceniceros:{
        type:Boolean,
        default:false
    },
    cinturones:{
        type:Boolean,
        default:false
    },
    botonesInteriores:{
        type:Boolean,
        default:false
    },
    manijasInteriores:{
        type:Boolean,
        default:false
    },
    tapetes:{
        type:Boolean,
        default:false
    },
    vestidura:{
        type:Boolean,
        default:false
    }
});

VehiculoInterioresSchema.method('toJSON', function(){
    const { _id, __v, ...objects } = this.toObject();
    objects.uid = _id;
    return objects;
});

module.exports = model('Interiores', VehiculoInterioresSchema, 'interiores');