const {Schema, model} = require('mongoose');

const FiscaliaSchema = Schema({
    idadminpaq: {
        type: String, 
        required: true
    },
    idcliente: {
        type: String, 
        required: true
    },
    idagente:{
        type: String, 
        required: true
    },
    folio: {
        type: String, 
        required: true
    },
    fecha: {
        type: String, 
        required: true
    },
    fecha_vencimiento: {
        type: String, 
        required: true
    },
    total: {
        type: String, 
        required: true
    },
    pendiente:{
        type: String, 
        required: true
    },
    fecha_server: {
        type: String, 
        required: true
    },
    status: {
        type: String, 
        required: true
    },
    estatus_documento: {
        type: String, 
        required: true
    },
    observaciones:{
        type: String, 
        required: true
    }
});

FiscaliaSchema.method('toJSON', function(){
    const { _id, __v, ...objects } = this.toObject();
    objects.uid = _id;
    return objects;
});

module.exports = model('Fiscalia',FiscaliaSchema,'fiscalia');
