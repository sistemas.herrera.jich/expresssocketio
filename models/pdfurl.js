const { Schema, model } = require('mongoose');

const PdfUrlSchema = Schema({
    urlentrada:{
        type:String,
        required:true
    },
    urlsalida:{
        type:String,
        default: ''
    },
    servicio:{
        type:String,
        default:''
    },
    observacion:{
        type:String,
        default:''
    },
    createAt:{
        type:Date,
        default: Date.now()
    }
});

PdfUrlSchema.method('toJSON', function(){
    const { __v, _id, ...object } = this.toObject();
    object.uid = _id;

    return object;
});

module.exports = model('PDF',PdfUrlSchema,'pdf');