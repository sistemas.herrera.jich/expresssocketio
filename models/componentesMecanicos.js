const { Schema, model } = require('mongoose');

ComponentesMecanicosSchema = Schema({
    claxon:{
        type: Boolean,
        default: false
    },
    taponAceite:{
        type:Boolean,
        default:false 
    },
    taponRadiador:{
        type:Boolean,
        default:false
    },
    varillaAceite:{
        type:Boolean,
        default:false
    },
    catalizador:{
        type:Boolean,
        default:false
    },
    bateria:{
        type:Boolean,
        default:false
    }
});

ComponentesMecanicosSchema.method('toJSON', function(){
    const { _id, __v, ...objects } = this.toObject();
    objects.uid = _id;
    return objects;
});

module.exports = model('Componentes', ComponentesMecanicosSchema, 'componentes');
