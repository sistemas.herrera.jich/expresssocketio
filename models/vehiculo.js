const { Schema, model } = require('mongoose');

VehiculoSchema = Schema({
    nombre:{
        type:String,
        required:true,
    },
    telefono:{
        type:String,
        required:false,
        default: 'N/A'
    },
    rfc:{
        type:String,
        required:false,
        default:'XAXX010101000'
    },
    modelo:{
        type:String,
        default: 'N/A'
    },
    tipo:{
        type:String,
        default: 'N/A'
    },
    marca:{
        type:String,
        default: 'N/A'
    },
    color:{
        type:String,
        default: 'N/A'
    },
    placa:{
        type:String,
        default: 'N/A'
    },
    nseries:{
        type:String,
        default: 'N/A'
    },
    nmotor:{
        type:String,
        default: 'N/A'
    },
    kmrecorridos:{
        type:String,
        default:'0.0'
    },
    fechaIngreso:{
        type:Date,
        default:Date.now
    },
    fechaEntrega:{
        type:Date,
        require:false
    },
    usuario:{
        type: Schema.Types.ObjectId,
        ref: 'Usuario'
    },
    exteriores:{
        type: Schema.Types.ObjectId,
        ref: 'Exteriores'
    },
    interiores:{
        type: Schema.Types.ObjectId,
        ref: 'Interiores'
    },
    accesorios:{
        type: Schema.Types.ObjectId,
        ref: 'Accesorios'
    },
    componentes:{
        type: Schema.Types.ObjectId,
        ref: 'Componentes'
    },
    orderservicio:{
        type: Schema.Types.ObjectId,
        ref:'Orden'
    },
    pdf:{
        type: Schema.Types.ObjectId,
        ref:'PDF'
    },
    ordencompra:{
        type: Schema.Types.ObjectId,
        ref:'OrdenCompra'
    },
    etapa:{
        type:Number,
        default:1
    }
});

VehiculoSchema.method('toJSON', function() {
    const { __v, _id, ...object } = this.toObject();

    object.uid = _id;
    return object;
});

module.exports = model('Vehiculo',VehiculoSchema,'vehiculo');