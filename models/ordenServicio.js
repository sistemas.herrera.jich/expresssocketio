const { Schema, model } = require('mongoose');

const OrdenServicioSchema = Schema({
    fecha:{
        type:Date,
        default:Date.now()
    },
    trabajoRealizar:{
        type:String,
        required:true
    },
    diagnostico:{
        type:String,
        required:true
    },
    riesgos:{
        type:String,
        required:true
    }
});

OrdenServicioSchema.method('toJSON', function(){
    const { _id, __v, ...objects } = this.toObject();
    objects.uid = _id;
    return objects;
});

module.exports = model('Orden',OrdenServicioSchema,'orden');