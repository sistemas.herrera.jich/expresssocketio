const {Schema, model} = require('mongoose');

const OrdenCompraSchema = Schema({
    ordenes:[{type: String}]
});

OrdenCompraSchema.method('toJSON', function() {
    const { __v, _id, ...object } = this.toObject();

    object.uid = _id;
    return object;
});

module.exports = model('OrdenCompra',OrdenCompraSchema,'ordencompra');