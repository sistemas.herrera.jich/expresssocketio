const { responce } = require('express');
const Usuario = require('../models/usuario');

const getUsuarios = async (req, res = response) => {
    const from = Number(req.query.from) || 0;

    //Find all usuarios
    const usuarios = await Usuario
    .find({ _id: { $ne: req.uid } })
    .sort('-online')
    .skip(from)
    .limit(10);

    res.json({
        ok: true,
        usuarios
    });

}

module.exports = {
    getUsuarios
}