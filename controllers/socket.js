const Usuario = require('../models/usuario');

const userConnection = async (uid = '') =>{

    const usuario = await Usuario.findById(uid);
    usuario.online = true;
    await usuario.save();
    return usuario;

}

const userDisconnect = async (uid = '') =>{

    const usuario = await Usuario.findById(uid);
    usuario.online = false;
    await usuario.save();
    return usuario;

}

module.exports = {
    userConnection,
    userDisconnect
}