const Vehiculo = require('../models/vehiculo');
const Orden = require('../models/ordenCompra');
const PDF = require('../models/pdfurl');
const Fiscalia = require('../models/facturasFiscalia');
const fs = require('fs');
const s3 = require('../config/bucket');
const path = require('path');
const axios = require('axios');

const getVehiculoEtapas = async (payload) => {
    try {
        const uid = payload.uid;
        const etapa = payload.etapa;

        if (uid === '') {
            return {ok:false, message: "Error al traer datos"}
        }

        if (etapa === 1) {
            const vehiculo = await Vehiculo.findOne({_id:uid});
            return vehiculo;
        }

        if (etapa === 2) {
            const vehiculo = await Vehiculo.findOne({_id:uid}).populate('exteriores').exec();
            return vehiculo;
        }

        if (etapa === 3) {
            const vehiculo = await Vehiculo.findOne({_id:uid})
            .populate('exteriores')
            .populate('interiores').exec();

            return vehiculo; 
        }

        if (etapa === 4) {
            const vehiculo = await Vehiculo.findOne({_id:uid})
            .populate('exteriores')
            .populate('interiores')
            .populate('accesorios')
            .exec();
            
            return vehiculo; 
        }

        if (etapa === 5) {
            const vehiculo = await Vehiculo.findOne({_id:uid})
            .populate('exteriores')
            .populate('interiores')
            .populate('accesorios')
            .populate('componentes')
            .exec();
            
            res.json({
                ok:true,
                vehiculo
            }); 
        }

        if (etapa === 6) {
            const vehiculo = await Vehiculo.findOne({_id:uid})
            .populate('exteriores')
            .populate('interiores')
            .populate('accesorios')
            .populate('componentes')
            .populate('orderservicio')
            .exec();
            
            res.json({
                ok:true,
                vehiculo
            });
        }

    } catch (error) {
        console.log(error);
        return res.status(400).json({
            ok:false,
            msg:'Error comuniquese con administrador'
        });
    }
};

const getVehiculos = async (req, res) =>{
    try {
        const uid = req.params.uid;

        const vehiculo = await Vehiculo.find({$and:[{etapa:6}, {usuario:uid} ]})
        .populate('exteriores')
        .populate('interiores')
        .populate('accesorios')
        .populate('componentes')
        .populate('orderservicio')
        .populate({path:'usuario', populate:{path:'sucursal'}})
        .populate('pdf')
        .exec();
        
        res.json({
            ok:true,
            vehiculo
        });
        
    } catch (error) {
        console.log(error);
        return res.status(400).json({
            ok:false,
            msg:'Error comuniquese con administrador'
        });
    }
}

const getVehiculosHistorial = async (req, res) => {
    const uid = req.uid;
    try {
        const vehiculo = await Vehiculo.find({$and:[{etapa:6}]}).sort({fechaIngreso: -1})
        .populate('exteriores')
        .populate('interiores')
        .populate('accesorios')
        .populate('componentes')
        .populate('orderservicio')
        .populate({path:'usuario', populate:{path:'sucursal'}})
        .populate('pdf')
        .exec();

        res.json({
            ok:true,
            vehiculo
        });

    } catch (error) {
        console.log(error);
        return res.status(400).json({
            ok:false,
            msg:'Error comuniquese con administrador'
        });
    }
}

const saveFileOrdenServicio = async (req, res) => {
    try {
        const {uid, opc, filename } = req.body;
        //fs.writeFileSync(path.resolve(__dirname, `../${uid}entrada.pdf`), buff);

        if(opc === ''){

            var data = {
                Bucket:process.env.BUCKET_NAME,
                Key:`${uid}entrada`,
                Body: filename,
                ContentType: 'text/plain'
            }
            
            let location = '';
            const { Location } = await s3.upload(data).promise();
            location = Location;

            const obj = {
                urlentrada: location
            }

            const pdf = new PDF(obj);
            pdf.save();

            const updateVehiculo = { pdf:pdf.id};
            await Vehiculo.updateOne({_id:uid},{$set:updateVehiculo});

            const vehiculo = await Vehiculo.find({_id:uid}).populate('pdf').exec();

            res.json({
                ok: true,
                vehiculo
            });
        }else{
            const {servicios, observaciones, ordenes} = req.body;

            var data = {
                Bucket:process.env.BUCKET_NAME,
                Key:`${uid}salida`,
                Body: filename,
                ContentType: 'text/plain'
            }
            
            let location = '';
            const { Location } = await s3.upload(data).promise();
            location = Location;

            const obj = {
                urlsalida:location,
                servicio:servicios,
                observacion:observaciones
            }


            await PDF.updateOne({_id:opc},{$set:obj});
            const pdf = await PDF.findById(opc);

            const orden = new Orden({ordenes});
            orden.save();
            
            const updateVehiculo = { ordencompra:orden.id};
            await Vehiculo.updateOne({_id:uid},{$set:updateVehiculo});

            const vehiculo = await Vehiculo.find({pdf:pdf.id}).populate('pdf').exec();

            res.json({
                ok: true,
                vehiculo
            });
        }
    } catch (error) {
        throw new Error("No se pudo guardar el PDF correctamente"+ error)
    }
};

const updatePdf = async (req, res) => {
    try {
        const {uid, filename, opc } = req.body;
        var data = {
            Bucket:process.env.BUCKET_NAME,
            Key:`${uid+opc}`,
            Body: filename,
            ContentType: 'text/plain'
        }
        
        let location = '';
        const { Location } = await s3.upload(data).promise();
        location = Location;

        if(opc === 'entrada'){
            let obj = {
                urlentrada : location
            }
            await PDF.updateOne({_id:uid},{$set:obj});
        }else{
            let obj = {
                urlsalida : location
            }
            await PDF.updateOne({_id:uid},{$set:obj});
        }

        res.json({
            ok: true,
            msg: 'pdf correctamente actulizado'
        });

    } catch (error) {
        console.log(error);
    }
}

const getPdfView = async (req, res)=>{

    try {

        const { uid, opc } = req.body;

        if(opc === 'entrada'){

            const filename = await PDF.findOne({_id:uid});
            let base64Pdf = await axios.get(filename.urlentrada);
            base64Pdf = await base64Pdf.data;
            const buff=Buffer.from(base64Pdf,'base64');
            // res.setHeader('Content-disposition', 'attachment; filename=file.pdf')
            res.setHeader('Content-type', 'application/pdf');
            res.send(buff);

        }else{

            const filename = await PDF.findById(uid);
            let base64Pdf = await axios.get(filename.urlsalida);
            base64Pdf = await base64Pdf.data;
            const buff=Buffer.from(base64Pdf,'base64');
            // res.setHeader('Content-disposition', 'attachment; filename=file.pdf')
            res.setHeader('Content-Type', 'application/pdf');
            res.send(buff);

        }

    } catch (error) {
        console.log(error)
    }

}

const makeDocumentosFiscalia = async (req, res) => {
    try {

        const response = await axios.post('https://jhksystem.com/API/documentosfiscalia/listadoDocumentos',{ "id": 1156 });
        
        await response.data.forEach(async(resp) => {

            const isExist = await Fiscalia.findOne({"folio":resp.folio});

            if(!isExist){
                const fiscalia = new Fiscalia(resp);
                fiscalia.save();
            }

        });

        res.json({ok:true});

    } catch (error) {
        res.json({ok:false});
        console.log(error);
    }
}

const getFiscalia = async (req, res) => {
    try {
        const fiscalia = await Fiscalia.find();

        res.json({
            ok: true,
            fiscalia
        });

    } catch (error) {
        console.log(error);
    }
}

module.exports = {
    getVehiculos,
    getVehiculosHistorial,
    saveFileOrdenServicio,
    getPdfView,
    updatePdf,
    makeDocumentosFiscalia,
    getFiscalia
}