const { response } = require("express");
const bcrypt = require('bcryptjs');
const { validationResult } = require("express-validator");

const { generarJWT } = require('../helpers/jwt')
const Usuario = require('../models/usuario')

const crearUsuario = async (req, res = response) => {

    const { email, password } = req.body;
    try {

        const isExistEmail = await Usuario.findOne({email});
        if( isExistEmail ){
            return res.status(400).json({
                ok:false,
                msg:'Credenciales incorrectas'
            });
        }

        const usuario = new Usuario(req.body);
        //Encriptar password
        const salt=bcrypt.genSaltSync();
        usuario.password = bcrypt.hashSync(password,salt);
        await usuario.save();

        const token = await generarJWT(usuario.id)

        res.json({
            ok:true,
            usuario,
            token
        });
        
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok:false,
            msg:'Comuniquece con el administrador'
        });
    }
}


const update = async ( req, res ) => {
    const { email, password } = req.body;
    const uid = req.params.uid;
    try {

        const isEmailExist = await Usuario.findOne({_id:uid});
        if( !isEmailExist ) {
            return res.status(400).json({
                ok:false,
                msg:"Las credenciales no son correctas"
            });
        }

        const salt = bcrypt.genSaltSync();
         req.body.password = bcrypt.hashSync(password,salt);

        await Usuario.updateOne({_id:uid},{$set: req.body});

        const result = await Usuario.findOne({_id:uid});

    
        res.json({
            ok:true,
            usuario:result,
        });
        
    } catch (error) {
        console.log(error);
        throw new Error("Error en la petición")
    }

};

const loginUsuario = async ( req, res ) => {
    const { email, password } = req.body;
    try {

        const isExistUser = await Usuario.findOne({email});
        if( !isExistUser ){
            return res.status(400).json({
                ok:false,
                msg:'Email incorrecto'
            });
        }

        const validPassword = bcrypt.compareSync( password, isExistUser.password );
        if( !validPassword ){
            return res.status(400).json({
                ok:false,
                msg:'Password no fue aceptado'
            });
        }
        
        const usuario = await Usuario.findOne({email}).populate('sucursal').exec();
        const token = await generarJWT(isExistUser.id)

        res.json({
            ok:true,
            usuario,
            token
        });

    } catch (error) {
        res.status(500).json({
            ok:false,
            msg:"Hable con el administrador"
        })
    }
}

const renewToken = async( req, res = response ) => {
    const uid = req.uid;

    try {
        const usuario = await Usuario.findById(uid).populate('sucursal').exec();

        if( !usuario ){
            return res.status(400).json({
                ok:false,
                msg:'Data incorrecta'
            });
        }

        const token = await generarJWT(usuario.id);

        res.json({
            ok:true,
            usuario,
            token
        });

    } catch (error) {

        return res.status(400).json({
            ok:false,
            msg:'Error comuniquese con administrador'
        });
    }
    
}

module.exports={
    crearUsuario,
    update,
    loginUsuario,
    renewToken
}
