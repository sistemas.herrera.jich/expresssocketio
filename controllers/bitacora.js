const Vehiculo = require('../models/vehiculo');

const bitacora ={};

bitacora.getVehiculosHistorialPaginator = async (req, res) => {
    const uid = req.uid;
    try {
        let filters = req.body.filters?req.body.filters:[];
        let limits=10,
            page = req.body.page > 0 ? req.body.page :0;
        let count_page = 0;

        const vehiculo = await Vehiculo.find({$and:filters})
        .sort({fechaIngreso: -1})
        .limit(limits)
        .skip(limits * page)
        .populate('exteriores')
        .populate('interiores')
        .populate('accesorios')
        .populate('componentes')
        .populate('orderservicio')
        .populate({path:'usuario', populate:{path:'sucursal'}})
        .populate('pdf')
        .exec();

        let count = await Vehiculo.countDocuments();

        count_page = count / limits;

        res.json({
            ok:true,
            vehiculo,
            count_page
        });

    } catch (error) {
        console.log(error);
        return res.status(400).json({
            ok:false,
            msg:'Error comuniquese con administrador'
        });
    }
}

module.exports = bitacora;