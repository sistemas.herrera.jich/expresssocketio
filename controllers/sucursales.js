const Sucursal = require('../models/sucursales');

const create = async ( req, res ) => {
    const { nombre } = req.body;

    try {

        const isExistSucursal = await Sucursal.findOne({nombre});
        
        if( isExistSucursal ) {
            return res.status(400).json({
                ok:false,
                msg:"No se permite duplicar el rfc"
            });
        }
        
        const sucursal = new Sucursal(req.body);
        sucursal.save();

        res.json({
            ok:true,
            sucursal
        });

    } catch (error) {
        console.log(error);
        throw new Error("Error al crear sucursal");
    }
};

const getSucursales = async (req, res) => {
    try {

        const sucursales = await Sucursal.find();
        console.log(sucursales);
        res.json({
            ok:true,
            sucursales
        });

    } catch (error) {
        
    }
}

module.exports = {
    create,
    getSucursales
}