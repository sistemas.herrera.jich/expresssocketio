const Vehiculo = require('../models/vehiculo');
const Exteriores = require('../models/vehiculoExteriores');
const Interiores = require('../models/vehiculoInteriores');
const Accesorios = require('../models/accesorios');
const Componentes = require('../models/componentesMecanicos');
const OrdenServicio = require('../models/ordenServicio');
const validation = require('../middlewares/validar-datos');

const vehiculoAdd = async (payload) => {
    await validation.validatorBucle(payload);

    if(payload.uid === ''){
        try {
            const vehiculo = new Vehiculo(payload);
            vehiculo.save();
            return {
                ok:true,
                vehiculo
            };
        } catch (error) {
            return {ok:false, message: "Error al agregar datos inciales"};
        }
    }else{
        uid = payload.uid
       try {
            await Vehiculo.updateOne({_id:uid},{$set: payload});
            const vehiculo = await Vehiculo.findOne({_id:uid});

            return {
                ok:true,
                vehiculo
            };
       } catch (error) {
            return {ok:false, message: "Error al actulizar datos"};
       }
    }
    
};

const exterioresAdd = async (payload) =>{
    try {
        if (payload.uid === '') {
            const uidVehiculo = payload.uidVehiculo;
            const exteriores = new Exteriores(payload);
            exteriores.save();

            //Relación update exteriores vehiculo
            const uidExteriores = exteriores.id;
            const updateVehiculo = { exteriores:uidExteriores, etapa:2};
            await Vehiculo.updateOne({_id:uidVehiculo},{$set: updateVehiculo});
            
            const vehiculo = await Vehiculo.findOne({_id:uidVehiculo}).populate('exteriores').exec();

            return vehiculo;
        } else {
            uid = payload.uid;
            const uidVehiculo = payload.uidVehiculo;
            await Exteriores.updateOne({_id:uid},{$set:payload});
          
            const vehiculo = await Vehiculo.findOne({_id:uidVehiculo}).populate('exteriores').exec();

            return vehiculo;
        }
    } catch (error) {
        console.log(error);
        return {ok:false, message: "Error al agregar datos exteriores"};
    }
};

const interioresAdd = async (payload) =>{
    try {
        if (payload.uid === '') {
            const uidVehiculo = payload.uidVehiculo;
            const interiores = new Interiores(payload);
            interiores.save();

            const uidInteriores = interiores.id;
            const updateVehiculo = { interiores:uidInteriores, etapa:3};
            await Vehiculo.updateOne({_id:uidVehiculo},{$set: updateVehiculo});
            
            const vehiculo = await Vehiculo.findOne({_id:uidVehiculo}).populate('interiores').exec();

            return vehiculo;

        } else {
            uid = payload.uid;
            const uidVehiculo = payload.uidVehiculo;
            await Interiores.updateOne({_id:uid},{$set: payload});
            const vehiculo = await Vehiculo.findOne({_id:uidVehiculo}).populate('interiores').exec();

            return vehiculo;
        }
    } catch (error) {
        console.log(error);
        return {ok:false, message: "Error al agregar datos exteriores"};
    }
};

const accesoriosAdd = async (payload) =>{
    try {
        if (payload.uid === '') {
            const uidVehiculo = payload.uidVehiculo;
            const accesorios = new Accesorios(payload);
            accesorios.save();

            const uidAccesorios = accesorios.id;
            const updateVehiculo = { accesorios:uidAccesorios,etapa:4 };
            await Vehiculo.updateOne({_id:uidVehiculo},{$set: updateVehiculo});
            
            const vehiculo = await Vehiculo.findOne({_id:uidVehiculo}).populate('interiores').exec();

            return vehiculo;

        } else {
            uid = payload.uid;
            const uidVehiculo = payload.uidVehiculo;
            await Accesorios.updateOne({_id:uid},{$set: payload});
            const vehiculo = await Vehiculo.findOne({_id:uidVehiculo}).populate('interiores').exec();

            return vehiculo;
        }
    } catch (error) {
        return {ok:false, message: "Error al agregar datos exteriores"};
    }
};

const componentesAdd = async (payload) =>{
    try {
        const uid = payload.uid;

        if (uid === '') {
            const componentes = new Componentes(payload);
            componentes.save();
            
            const uidComponentes = componentes.id;
            const uidVehiculo = payload.uidVehiculo;
            const updateVehiculo = { componentes:uidComponentes, etapa:5};
            await Vehiculo.updateOne({_id:uidVehiculo},{$set:updateVehiculo});

            const vehiculo = await Vehiculo.findOne({_id:uidVehiculo}).populate('componentes').exec();

            return vehiculo; 
        } else {
            const uidVehiculo = payload.uidVehiculo;
            await Componentes.updateOne({_id:uid},{$set: payload});
            const vehiculo = await Vehiculo.findOne({_id:uidVehiculo}).populate('interiores').exec();

            return vehiculo;
        }
    } catch (error) {
        console.log(error);
        return {ok:false, message: "Error al agregar datos exteriores"};
    }
};

const ordenServicio = async (payload) =>{
    try {
        const uid = payload.uid;

        if (uid === '') {

            const ordenServicio = new OrdenServicio(payload);
            ordenServicio.save();
            
            const uidOrden = ordenServicio.id;
            const uidVehiculo = payload.uidVehiculo;
            const updateVehiculo = { orderservicio:uidOrden, etapa:6 };
            await Vehiculo.updateOne({_id:uidVehiculo},{$set:updateVehiculo});

            const vehiculo = await Vehiculo.findOne({_id:uidVehiculo}).populate('ordenservicio').exec();

            return vehiculo;

        } else {
            await Componentes.updateOne({_id:uid},{$set: payload});
            const uidVehiculo = payload.uidVehiculo;
            const vehiculo = await Vehiculo.findOne({_id:uidVehiculo}).populate('ordenservicio').exec();

            return vehiculo;

        }
    } catch (error) {
        return {ok:false, message: "Error al agregar datos exteriores"};
    }
};

const getVehiculoEtapas = async (payload) => {
    try {
        const uid = payload.uid;
        const etapa = payload.etapa;

        if (uid === '') {
            return {ok:false, message: "Error al traer datos"}
        }

        if (etapa === 1) {
            const vehiculo = await Vehiculo.findOne({_id:uid});
            return vehiculo;
        }

        if (etapa === 2) {
            const vehiculo = await Vehiculo.findOne({_id:uid}).populate('exteriores').exec();
            return vehiculo;
        }

        if (etapa === 3) {
            const vehiculo = await Vehiculo.findOne({_id:uid})
            .populate('exteriores')
            .populate('interiores').exec();

            return vehiculo; 
        }

        if (etapa === 4) {
            const vehiculo = await Vehiculo.findOne({_id:uid})
            .populate('exteriores')
            .populate('interiores')
            .populate('accesorios')
            .exec();
            
            return vehiculo; 
        }

        if (etapa === 5) {
            const vehiculo = await Vehiculo.findOne({_id:uid})
            .populate('exteriores')
            .populate('interiores')
            .populate('accesorios')
            .populate('componentes')
            .exec();
            
            return vehiculo; 
        }

        if (etapa === 6) {
            const vehiculo = await Vehiculo.findOne({_id:uid})
            .populate('exteriores')
            .populate('interiores')
            .populate('accesorios')
            .populate('componentes')
            .populate('orderservicio')
            .exec();
            
            return vehiculo; 
        }

    } catch (error) {
        console.log(error);
        return {ok:false, message: "Error al traer datos"}
    }
};

const getVehiculos = async () =>{
    try {
      
        const vehiculo = await Vehiculo.find();
        return vehiculo;
        
    } catch (error) {
        console.log(error);
        return {ok:false, message: "Error al traer datos"}
    }
}

module.exports = {
    vehiculoAdd,
    exterioresAdd, 
    interioresAdd, 
    accesoriosAdd, 
    componentesAdd,
    ordenServicio,
    getVehiculoEtapas,
    getVehiculos
}
